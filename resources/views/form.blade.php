<!DOCTYPE html>
<html>
    <head>
        <title>Form</title>
        <meta charset="UTF-8">
    </head>

    <body>
        <!--Heading-->
        <h2>Buat Account Baru!</h2>
        <h4>Sign Up Form</h4>

        <!--Form-->
        <form action="/welcome" method="POST">
            @csrf
            <!-- <label for="fnama">First Name:<br><br></label> -->
            <p>First Name: </p>
            <input type="texs" name="first_nama">
            <!-- <label for="lnama">Last Name:<br><br></label> -->
            <p>Last Name: </p>
            <input type="texs" name="last_nama">
            <!-- <label>Gender:<br><br></label> -->
            <p>Gender: </p>
            <input type="radio" name="user_gender" value="0">Male<br>
            <input type="radio" name="user_gender" value="1">Female<br>
            <input type="radio" name="user_gender" value="2">Other<br>
            <p>Nationality</p>
            <select name="negara">
                <option value="0">Indonesian</option>
                <option value="1">Singaporean</option>
                <option value="2">Malaysian</option>
                <option value="3">Australian</option>
            </select>
            <p>Language Spoken</p>
            <input type="checkbox" name="language" value="0">Bahasa Indonesia<br>
            <input type="checkbox" name="language" value="1">English<br>
            <input type="checkbox" name="language" value="2">Other<br>
            <p>Bio</p>
            <textarea cols="30" rows="10" id="bio"></textarea><br>
            <input type="submit" value="Sign Up">
        </form>
    </body>

</html>