<!DOCTYPE html>
<html>
    <head>
        <title>SanberBook</title>
        <meta charset="UTF-8">
    </head>

    <body>

        <!--Bagian heading-->
        <div>
            <h1>SanberBook</h1>
            <h2>Social Media Developer Santai Berkualitas</h2>
        </div>

        <!--Konten website-->
        <div>
            <p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>
            <h3>Benefit Join di SanberBook</h3>
            <!--Unordered list-->
            <ul>
                <li>Mendapatkan motivasi dari sesama developer</li>
                <li>Sharing knowledge dari para mastah Sanber</li>
                <li>Dibuat oleh calon web developer terbaik</li>
            </ul>

            <h3>Cara Bergabung ke SanberBook</h3>
            <!--Ordered list-->
            <ol>
                <li>Mengunjungi Website ini</li>
                <li>Mendaftar di <a href="/register">Form Sign Up</a></li>
                <li>Selesai!</li>
            </ol>
        </div>

    </body>

</html>